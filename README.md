# EmoteVille

This repo is serving as a public issue tracker for EmoteVille.

If you are experiencing an issue on the server then please
submit an issue at https://gitlab.com/emotemc/issue-tracker/-/issues
